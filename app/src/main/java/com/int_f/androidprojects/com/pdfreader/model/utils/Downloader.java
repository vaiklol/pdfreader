package com.int_f.androidprojects.com.pdfreader.model.utils;

import android.app.NotificationManager;

import java.io.File;


/**
 * Created by user on 11.03.2018.
 */

public interface Downloader {

    void downloadFromUrl(NotificationManager m, String... arg);
    void getFilesFromFolder(File path);
}
