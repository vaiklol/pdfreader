package com.int_f.androidprojects.com.pdfreader.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.github.barteksc.pdfviewer.PDFView;
import com.int_f.androidprojects.com.pdfreader.R;
import com.int_f.androidprojects.com.pdfreader.abstracts.FragmentReader;
import com.int_f.androidprojects.com.pdfreader.model.PDFdoc;
import com.int_f.androidprojects.com.pdfreader.services.FirebaseFetchService;
import com.int_f.androidprojects.com.pdfreader.view.utils.VFragmensCallback;
import com.int_f.androidprojects.com.pdfreader.view.viewAdapters.ArrayListAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * View class with pdfs list and download button
 */

public class PDFListFragment extends FragmentReader {

    private static final String REQUEST_PDFS = "pi";
    private ArrayList<PDFdoc> mPDFdocs;
    private ListView mListView;
    private static final String TAG = "PDFListFragment";
    private CardView mCardView;
    
    public static PDFListFragment getInstance(ArrayList<PDFdoc> list) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(REQUEST_PDFS, list);
        PDFListFragment listFragment = new PDFListFragment();
        listFragment.setArguments(args);
        return listFragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.pdfs_list;
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(FirebaseFetchService.BUTTON_ENABLE);
        getActivity().registerReceiver(mReceiver, filter, FirebaseFetchService.PRIVATE, null);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().getParcelableArrayList(REQUEST_PDFS) != null) {
            mPDFdocs = getArguments().getParcelableArrayList(REQUEST_PDFS);
        } else {
            mPDFdocs = new ArrayList<>();
        }

        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mListView = v.findViewById(R.id.pdfs_list);
        final ArrayListAdapter arrayListAdapter = new ArrayListAdapter(getActivity(), android.R.layout.simple_list_item_1, mPDFdocs);
        mListView.setAdapter(arrayListAdapter);
        mListView.setOnItemClickListener(new OnDocClickListener());
        mCardView = v.findViewById(R.id.downloader);
        mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VFragmensCallback<String> callback = (VFragmensCallback<String>) getActivity();
                if (isNetWorkEnabled()) {
                    callback.fetchNewData();
                    mCardView.animate().translationY(mCardView.getHeight()).setInterpolator(new LinearInterpolator())
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    mCardView.setEnabled(false);
                                }
                            }).start();
                } else {
                    Snackbar.make(v, "No internet connection", Snackbar.LENGTH_SHORT)
                            .setAction("Open settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                    startActivity(intent);
                                }
                            }).show();
                }
            }
        });
        return v;
    }

    private class OnDocClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            VFragmensCallback<String> callback = (VFragmensCallback<String>) getActivity();
            callback.addNewFragment(mPDFdocs.get(position).getPDFullName(), mPDFdocs.get(position).isFromAssets());

        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d(TAG, "onViewStateRestored: ");
    }

    public ListView getListView() {
        return mListView;
    }

    private boolean isNetWorkEnabled() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isOnline = connectivityManager.getActiveNetworkInfo() != null &&
                connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
        return isOnline;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ArrayListAdapter)mListView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(mReceiver);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mCardView.setEnabled(true);
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            DisplayMetrics metrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(metrics);
            mCardView.animate().translationY(0).setInterpolator(new LinearInterpolator())
                    .start();
        }
    };
}

