package com.int_f.androidprojects.com.pdfreader.model.utils;

/**
 * Created by user on 12.03.2018.
 */

public interface DataObserver<T> {
    void modelChanged(T t);
}
