package com.int_f.androidprojects.com.pdfreader.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;

import com.github.barteksc.pdfviewer.PDFView;
import com.int_f.androidprojects.com.pdfreader.interfaces.Exchanger;
import com.int_f.androidprojects.com.pdfreader.model.utils.DataObserver;
import com.int_f.androidprojects.com.pdfreader.model.utils.NameSpliterator;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 * Lab class for thats contain main data about pdfs files
 */

public class PDFLab implements Exchanger<String>, Parcelable {
    private Stack<PDFdoc> mPDFStack;
    private ArrayList<PDFdoc> mPDFList;
    private HashMap<String, PDFdoc> mAssetsDocs;
    private HashMap<String, PDFdoc> mFilesDoc;
    transient private static final String TAG = "PDFLab";
    private AssetsLoader mAssetsLoader;
    private FirebaseDownloader mFirebaseDownloader;
    private DataObserver<PDFdoc> mObserver;
    private FilesLoader mFilesLoader;

    public PDFLab(Context context) {
        File file = new File(context.getFilesDir(), "pdfs/");
        if (!file.exists()) {
            file.mkdir();
        }
        mPDFStack = new Stack<>();
        mPDFList = new ArrayList<>();
        mAssetsDocs = new HashMap<>();
        mAssetsLoader = new AssetsLoader(context);
        mFilesDoc = new HashMap<>();
        mAssetsLoader.setExchanger(this);
        mFilesLoader = new FilesLoader(context, mAssetsLoader.getListNameSpliterator());
        mFilesLoader.startWatching();
        mFirebaseDownloader = new FirebaseDownloader(context);
    }

    public void setObserverForFiles(DataObserver<PDFdoc> observer) {
        mObserver = observer;
        mFilesLoader.loadFiles(null);
    }

    protected PDFLab(Parcel in) {
        mPDFList = in.createTypedArrayList(PDFdoc.CREATOR);
        mAssetsDocs = in.readHashMap(HashMap.class.getClassLoader());
        in.readList(mPDFStack, Stack.class.getClassLoader());
    }

    /**
     * method to start downloading service form object with given path
     * first - pdfs zip name
     * second url path from storage
     */
    public void downLoadDataFromFB() {
        mFirebaseDownloader.downloadFromUrl("workerManager", "pdfsWorker/");
    }

    public static final Creator<PDFLab> CREATOR = new Creator<PDFLab>() {
        @Override
        public PDFLab createFromParcel(Parcel in) {
            return new PDFLab(in);
        }

        @Override
        public PDFLab[] newArray(int size) {
            return new PDFLab[size];
        }
    };

    @Override
    public void dataLoaded(String s, String s2) {
        PDFdoc fdoc = new PDFdoc.PDFdocBuilder().setName(s2)
                .setFullName(s)
                .setCurrentPage(0)
                .buildDoc();
        if (mObserver != null) {
            mPDFList.add(fdoc);
            mObserver.modelChanged(fdoc);
            mFilesDoc.put(s2 + ".pdf", fdoc);
        } else {
            fdoc.setFromAssets(true);
            mPDFList.add(fdoc);
            mAssetsDocs.put(s2 + ".pdf", fdoc);
        }
    }

    public Stack<PDFdoc> getPDFStack() {
        return mPDFStack;
    }

    public ArrayList<PDFdoc> getPDFList() {
        return mPDFList;
    }

    public HashMap<String, PDFdoc> getFdocs() {
        return mAssetsDocs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mPDFList);
        dest.writeMap(mAssetsDocs);
        dest.writeList(mPDFStack);
    }

    public HashMap<String, PDFdoc> getFilesDoc() {
        return mFilesDoc;
    }

}
