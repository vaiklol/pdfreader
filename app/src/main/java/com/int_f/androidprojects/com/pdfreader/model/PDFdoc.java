package com.int_f.androidprojects.com.pdfreader.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.github.barteksc.pdfviewer.PDFView;

import java.util.Stack;

/**
 * Pdf file model
 */

public class PDFdoc implements Parcelable {
    private String mPDFname;
    private String mPDFullName;
    private int mCurrentPage;
    private Stack<Integer> mPagesStack;
    private boolean mFromAssets = false;
    private PDFdoc() {
        mPagesStack = new Stack<>();
    }

    public PDFdoc(Parcel in) {
        mPDFname = in.readString();
        mPDFullName = in.readString();
        in.readList(mPagesStack, Stack.class.getClassLoader());
        mFromAssets = in.readByte() == 1;
    }

    public static final Creator<PDFdoc> CREATOR = new Creator<PDFdoc>() {
        @Override
        public PDFdoc createFromParcel(Parcel in) {
            return new PDFdoc(in);
        }

        @Override
        public PDFdoc[] newArray(int size) {
            return new PDFdoc[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPDFullName);
        dest.writeString(mPDFname);
        dest.writeList(mPagesStack);
        int b = mFromAssets ? 1 : 0;
        dest.writeByte((byte) b);
    }

    protected static class PDFdocBuilder {
        private static String sName;
        private static String sFullName;
        private static int sCurrentPage;
        private static boolean sFromAssets;

        public PDFdocBuilder setName(String name) {
            sName = name;
            return this;
        }

        public PDFdocBuilder setFullName(String fullName) {
            sFullName = fullName;
            return this;
        }

        public PDFdocBuilder setCurrentPage(int page) {
            sCurrentPage = page;
            return this;
        }

        public PDFdocBuilder setFromAssets(boolean assets) {
            sFromAssets = assets;
            return this;
        }

        public PDFdoc buildDoc() {
            PDFdoc fdoc = new PDFdoc();
            fdoc.mPDFullName = PDFdocBuilder.sFullName;
            fdoc.mPDFname = PDFdocBuilder.sName;
            fdoc.mFromAssets = PDFdocBuilder.sFromAssets;
            return fdoc;
        }
    }

    public String getPDFname() {
        return mPDFname;
    }

    public String getPDFullName() {
        return mPDFullName;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        mCurrentPage = currentPage;
    }

    public Stack<Integer> getPagesStack() {
        return mPagesStack;
    }

    public void setFromAssets(boolean fromAssets) {
        mFromAssets = fromAssets;
    }

    public boolean isFromAssets() {
        return mFromAssets;
    }
}
