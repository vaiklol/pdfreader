package com.int_f.androidprojects.com.pdfreader.view.utils;

import com.int_f.androidprojects.com.pdfreader.model.PDFdoc;

/**
 * Callback interface for activity from views
 */

public interface VFragmensCallback<T> {
    void changeDoc(T t);
    void addNewFragment(T t, boolean b);
    void saveDocumentState(PDFdoc pdFdoc);
    void fetchNewData();
    void startVideo(T s);
}
