package com.int_f.androidprojects.com.pdfreader.model;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.int_f.androidprojects.com.pdfreader.interfaces.Exchanger;
import com.int_f.androidprojects.com.pdfreader.model.utils.NameSpliterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * class for loading files from assets
 */

class AssetsLoader {

    private ArrayList<String> mAssets;
    private Context mContext;
    private final static String mPath = "pdfs"; /** pdf storage in assets folder path */
    private static final String TAG = "AssetsLoader";
    private NameSpliterator<List<String>> mListNameSpliterator;

    AssetsLoader(Context context) {
        mContext = context;
        mAssets = new ArrayList<>();
        mListNameSpliterator = new NameSpliterator<>();
        scanAssets(mPath);
    }

    private List<String> getAssets() {
        return mAssets;
    }

    public NameSpliterator<List<String>> getListNameSpliterator() {
        return mListNameSpliterator;
    }

    private void splitNames() {
        mListNameSpliterator.splitNamesFromList(getAssets());
    }

    private void scanAssets(String path) {
        String[] arr;
        String aZz = path;
        try {
            AssetManager manager = mContext.getAssets();
            arr = manager.list(path);
            for (String s : arr) {
                if (s.matches("[^/]+|[\\S]?+|\\w+(.pdf)")) {
                    path = aZz + "/" + s;
                    mAssets.add(path);
                } else {
                    path = aZz + "/" + s;
                    scanAssets(path);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void setExchanger(Exchanger<String> exchanger) {
        mListNameSpliterator.setStringExchanger(exchanger);
        splitNames();
    }
}
