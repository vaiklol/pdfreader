package com.int_f.androidprojects.com.pdfreader.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.int_f.androidprojects.com.pdfreader.R;

/**
 * Dialog for sharing
 */

public class PDFShareDialog extends AlertDialog {

    private static final String TAG = "PDFShareDialog";

    protected PDFShareDialog(@NonNull Context context) {
        super(context);
    }

    public static AlertDialog newInstance(Context context, OnClickListener listener) {
        String[] p = new String[] {"Share page", "Share doc"};
        PDFShareDialog.Builder builder = new AlertDialog.Builder(context)
                .setCancelable(true)
                .setIcon(R.mipmap.pdf_icon)
                .setTitle("Choose share")
                .setItems(p, listener);
        return builder.create();
    }
}
