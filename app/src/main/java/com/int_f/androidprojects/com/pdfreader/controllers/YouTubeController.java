package com.int_f.androidprojects.com.pdfreader.controllers;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.int_f.androidprojects.com.pdfreader.R;

/**
 * YouTube activity
 */

public class YouTubeController extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final String TAG = "YouTubeController";
    
    private static final String KEY_API = "AIzaSyBqQYoa4HY6GhDljlmQ51Zk_Xs_zE88Yso";
    private String mUrl;
    public static final String REQUEST_URL = "com.int_f.androidprojects.com.pdfreader.controllers.URL";
    private YouTubePlayerFragment mPlayerFragment;
    private YouTubePlayer mTubePlayer;
    public static Intent getInstance(Context context, String s) {
        Intent intent = new Intent(context, YouTubeController.class);
        intent.putExtra(REQUEST_URL, s);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.you_tube_fragment);
        if (getIntent().getStringExtra(REQUEST_URL) != null) {
            mUrl = getIntent().getStringExtra(REQUEST_URL);
        }

        mPlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fr);
        mPlayerFragment.initialize(KEY_API, this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            mTubePlayer = youTubePlayer;
            mTubePlayer.cueVideo(mUrl);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Log.d(TAG, "onInitializationFailure: " + youTubeInitializationResult.name());
        youTubeInitializationResult.getErrorDialog(YouTubeController.this, 0).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
