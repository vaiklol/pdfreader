package com.int_f.androidprojects.com.pdfreader.model.utils;


import android.util.Log;
import com.int_f.androidprojects.com.pdfreader.interfaces.Exchanger;
import java.util.List;

/**
 * Class for splits names from given path
 */

public class NameSpliterator<T extends List<String>> {
    private T mFullNames;
    transient private static final String TAG = "NameSpliterator";
    Exchanger<String> mStringExchanger;

    public NameSpliterator<T> setStringExchanger(Exchanger<String> stringExchanger) {
        mStringExchanger = stringExchanger;
        return this;
    }

    private void splitNames() {
        for (String s : mFullNames) {
            String[] arr = s.split("\\.|[\\\\/]");
            mStringExchanger.dataLoaded(s, arr[arr.length - 2]);
            Log.d(TAG, "splitedName: " + arr[arr.length - 2]);
        }
    }

    public void splitNamesFromList(T names) {
        mFullNames = names;
        splitNames();
    }

    public static String splitName(String s) {
        String[] arr = s.split("[\\\\/]");
        return arr[arr.length - 1];
    }

}
