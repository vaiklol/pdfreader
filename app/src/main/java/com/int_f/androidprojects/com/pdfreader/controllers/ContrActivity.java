package com.int_f.androidprojects.com.pdfreader.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.int_f.androidprojects.com.pdfreader.R;
import com.int_f.androidprojects.com.pdfreader.abstracts.SingleFragmentActivity;
import com.int_f.androidprojects.com.pdfreader.controllers.controllersUtils.ControllerCallback;
import com.int_f.androidprojects.com.pdfreader.model.PDFLab;
import com.int_f.androidprojects.com.pdfreader.model.PDFdoc;
import com.int_f.androidprojects.com.pdfreader.model.utils.DataObserver;
import com.int_f.androidprojects.com.pdfreader.model.utils.NameSpliterator;
import com.int_f.androidprojects.com.pdfreader.view.PDFListFragment;
import com.int_f.androidprojects.com.pdfreader.view.PDFragment;
import com.int_f.androidprojects.com.pdfreader.view.utils.VFragmensCallback;
import com.int_f.androidprojects.com.pdfreader.view.viewAdapters.ArrayListAdapter;

import java.util.HashMap;
import java.util.List;

/**
 * MainActivity that controls attached fragments and change data from model
 */

public class ContrActivity extends SingleFragmentActivity implements VFragmensCallback<String>, DataObserver<PDFdoc> {

    private HashMap<String, PDFdoc> mPDFsFromAssets;
    private HashMap<String, PDFdoc> mPDFsFromFiles;
    private PDFLab mPDFLab;
    private PDFdoc mPDFdoc;
    private PDFListFragment mPDFListFragment;
    private PDFragment mPDFragment;
    private static final String TAG = "ContrActivity";
    private static final String PDF_DOC = "pdfragment";
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected int getContainer() {
        return R.id.fragment_container;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_contr;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mPDFLab = new PDFLab(this);
            mPDFLab.setObserverForFiles(this);
            mPDFListFragment = PDFListFragment.getInstance(mPDFLab.getPDFList());
        } else {
            mPDFLab = savedInstanceState.getParcelable(TAG);
            mPDFragment = (PDFragment) getFragmentFromBackStack(PDFragment.class.getSimpleName());
            mPDFdoc = savedInstanceState.getParcelable(PDF_DOC);
            mPDFListFragment = (PDFListFragment) getFragmentFromBackStack(PDFListFragment.class.getSimpleName());
        }
        mPDFsFromAssets = mPDFLab.getFdocs();
        mPDFsFromFiles = mPDFLab.getFilesDoc();
        addMainFragment();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


    }

    @Override
    protected Fragment getFragment() {
        return mPDFListFragment;
    }

    @Override
    protected Fragment changeFragment() {
        return mPDFragment;
    }

    @Override
    public void changeDoc(String s) {
        if (s != null) {
            s = NameSpliterator.splitName(s);
            if (mPDFsFromAssets.containsKey(s)) {
                mPDFdoc = mPDFsFromAssets.get(s);
                ControllerCallback<PDFdoc> controllerCallback = (ControllerCallback<PDFdoc>) mPDFragment;
                controllerCallback.changeFragmentData(mPDFdoc);
            } else if (mPDFsFromFiles.containsKey(s)) {
                mPDFdoc = mPDFsFromFiles.get(s);
                ControllerCallback<PDFdoc> controllerCallback = (ControllerCallback<PDFdoc>) mPDFragment;
                controllerCallback.changeFragmentData(mPDFdoc);
            }
        }
    }

    @Override
    public void addNewFragment(String s, boolean fromAsset) {
        s = NameSpliterator.splitName(s);
        if (fromAsset) {
            if (mPDFsFromAssets.containsKey(s) && mPDFragment == null) {
                mPDFdoc = mPDFsFromAssets.get(s);
                hideListFragment();
                mPDFragment = PDFragment.getInstance(mPDFdoc);
                addFragment();
            }
        } else {
            if (mPDFsFromFiles.containsKey(s) && mPDFragment == null) {
                mPDFdoc = mPDFsFromFiles.get(s);
                hideListFragment();
                mPDFragment = PDFragment.getInstance(mPDFdoc);
                addFragment();
            }
        }
    }

    @Override
    public void saveDocumentState(PDFdoc pdFdoc) {
        mPDFLab.getPDFStack().add(pdFdoc);
        Log.d(TAG, "saveDocumentState: " + mPDFLab.getPDFStack().size());
    }

    @Override
    public void fetchNewData() {
        mPDFLab.downLoadDataFromFB();
    }

    @Override
    public void startVideo(String s) {
        if (s != null && s.contains("youtu.be/")) {
            s = NameSpliterator.splitName(s);
            Intent intent = YouTubeController.getInstance(getApplicationContext(), s);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        ControllerCallback<PDFdoc> controllerCallback = (ControllerCallback<PDFdoc>) mPDFragment;
        if (!mPDFLab.getPDFStack().empty()) {
            mPDFdoc = mPDFLab.getPDFStack().pop();
            controllerCallback.onBackPressCallBack(mPDFdoc);
        } else {
            if (mPDFdoc != null) {
                if (mPDFdoc.getPagesStack().empty()) {
                    mPDFragment = null;
                    super.onBackPressed();
                    showListFragment();
                }
                else {
                 Integer page = mPDFdoc.getPagesStack().pop();
                 controllerCallback.changePageInPDFdoc(page);
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(TAG, mPDFLab);
        outState.putParcelable(PDF_DOC, mPDFdoc);
        if (mPDFragment != null) {
            getSupportFragmentManager().saveFragmentInstanceState(mPDFragment);
        }
    }

    private Fragment getFragmentFromBackStack(String name) {
        FragmentManager manager = getSupportFragmentManager();
        List<Fragment> list = manager.getFragments();
        Fragment target = null;
        for (Fragment fragment : list) {
            if (fragment.getClass().getSimpleName().equals(name)) {
                target = fragment;
            }
        }
        return target;
    }

    private void hideListFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .hide(mPDFListFragment)
                .commit();
    }

    private void showListFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.in_lefdt, android.R.animator.fade_out)
                .show(mPDFListFragment)
                .commit();
    }

    @Override
    public void modelChanged(final PDFdoc pdFdoc) {
        if (mPDFListFragment != null) {
            mPDFListFragment.getListView().post(new Runnable() {
                @Override
                public void run() {
                    ((ArrayListAdapter)mPDFListFragment.getListView().getAdapter()).notifyDataSetChanged();
                }
            });
        }
    }


}
