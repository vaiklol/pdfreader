package com.int_f.androidprojects.com.pdfreader.model;

import android.content.Context;
import android.content.Intent;

import com.int_f.androidprojects.com.pdfreader.services.FirebaseFetchService;



/**
 * Class for start downloading service
 */

public class FirebaseDownloader {

    private Context mContext;
    private static final String TAG = "FirebaseDownloader";
    public static final String PATHS = "pi";


    FirebaseDownloader(Context context) {
        mContext = context;
    }

    void downloadFromUrl(final String... arg) {
        Intent intent = new Intent(mContext, FirebaseFetchService.class);
        intent.putExtra(PATHS, arg);
        mContext.startService(intent);
    }

}
