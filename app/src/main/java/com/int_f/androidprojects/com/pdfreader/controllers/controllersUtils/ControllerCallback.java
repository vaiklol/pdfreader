package com.int_f.androidprojects.com.pdfreader.controllers.controllersUtils;

/**
 * Callbacks interface
 */

public interface ControllerCallback<T> {
    void changeFragmentData(T arg);
    void onBackPressCallBack(T arg);
    void changePageInPDFdoc(Integer arg);
}
