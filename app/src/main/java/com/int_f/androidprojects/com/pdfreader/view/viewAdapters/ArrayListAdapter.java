package com.int_f.androidprojects.com.pdfreader.view.viewAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.int_f.androidprojects.com.pdfreader.R;
import com.int_f.androidprojects.com.pdfreader.model.PDFLab;
import com.int_f.androidprojects.com.pdfreader.model.PDFdoc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 08.03.2018.
 */

public class ArrayListAdapter extends ArrayAdapter<PDFdoc> {

    private ArrayList<PDFdoc> mPDFdocs;

    public ArrayListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<PDFdoc> objects) {
        super(context, resource, objects);
        mPDFdocs = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pdfs_list_item, parent, false);
        }
        TextView view = convertView.findViewById(R.id.pdf_name_item);
        view.setText(mPDFdocs.get(position).getPDFname());
        return convertView;
    }

    @Override
    public int getCount() {
        return mPDFdocs.size();
    }

}
