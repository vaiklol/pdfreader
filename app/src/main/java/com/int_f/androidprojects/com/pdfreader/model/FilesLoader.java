package com.int_f.androidprojects.com.pdfreader.model;

import android.content.Context;
import android.os.FileObserver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.int_f.androidprojects.com.pdfreader.model.utils.DataObserver;
import com.int_f.androidprojects.com.pdfreader.model.utils.NameSpliterator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * class for loading files from app directory extended from FileObserver with pdfs files path
 */

class FilesLoader extends FileObserver {

    private static final String TAG = "FilesLoader";

    private Context mContext;
    private List<String> mStringArrayList;
    private NameSpliterator<List<String>> mListNameSpliterator;

    FilesLoader(Context context, NameSpliterator<List<String>> spliterator) {
        super(context.getFilesDir().getAbsolutePath() + "/pdfs/");
        mContext = context;
        mListNameSpliterator = spliterator;
        mStringArrayList = new ArrayList<>();
    }

    private void loadFilesFromStore(@Nullable String st) {
        File path = new File(mContext.getFilesDir(), "pdfs/");
        if (st == null) {
            String[] list = path.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    String s = name;
                    return s.contains(".pdf");
                }
            });
            if (list != null) {
                for (String s : list) {
                    mStringArrayList.add(path.getAbsolutePath() + "/" + s);
                }
                splitNames();
            }
        } else {
            File name = new File(path, st);
            if (name.isHidden()) {
                name.delete();
            } else if (name.getName().contains(".pdf")) {
                ArrayList<String> list = new ArrayList<>();
                list.add(name.getAbsolutePath());
                mStringArrayList = list;
                splitNames();
            }
        }

    }

    private void splitNames() {
        if (mStringArrayList != null) {
            mListNameSpliterator.splitNamesFromList(mStringArrayList);
        }
    }

    void loadFiles(@Nullable String s) {
        loadFilesFromStore(s);
    }

    @Override
    public void onEvent(int event, @Nullable String path) {
        if (event == FileObserver.CREATE) {
            loadFiles(path);
        }
    }
}
