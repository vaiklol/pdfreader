package com.int_f.androidprojects.com.pdfreader.abstracts;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.int_f.androidprojects.com.pdfreader.R;

/**
 * abstraction for activities
 */

public abstract class SingleFragmentActivity extends AppCompatActivity {

    @IdRes
    protected abstract int getContainer();
    @LayoutRes
    protected abstract int getContentView();

    protected abstract Fragment getFragment();
    protected abstract Fragment changeFragment();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
    }

    protected void addMainFragment() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(getContainer());
        if (fragment == null) {
            fragment = getFragment();
            manager.beginTransaction()
                    .add(getContainer(), fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
    }

    protected void addFragment() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(getContainer());
        if (fragment != null) {
            fragment = changeFragment();
            manager.beginTransaction()
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .setCustomAnimations(R.animator.in_lefdt, android.R.animator.fade_out)
                    .add(getContainer(), fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
    }
}
