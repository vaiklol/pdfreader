package com.int_f.androidprojects.com.pdfreader.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.int_f.androidprojects.com.pdfreader.R;
import com.int_f.androidprojects.com.pdfreader.model.FirebaseDownloader;
import com.int_f.androidprojects.com.pdfreader.model.utils.Downloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Service that starts downloading from firebase server and unzip pdfs files
 */

public class FirebaseFetchService extends Service implements Downloader {

    private static final String TAG = "FirebaseFetchService";
    private StepExecutor mStepExecutor;
    private LinkedBlockingQueue<Runnable> mRunnables;
    public static final String BUTTON_ENABLE = "com.int_f.androidprojects.com.pdfreader.services.ENABLE";
    public static final String PRIVATE = "com.int_f.androidprojects.com.pdfreader.PRIVATE";

    @Override
    public void onCreate() {
        super.onCreate();
        mRunnables= new LinkedBlockingQueue<>();
        mStepExecutor = new StepExecutor(mRunnables, getApplicationContext());

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getStringArrayExtra(FirebaseDownloader.PATHS) != null) {
            String[] args = intent.getStringArrayExtra(FirebaseDownloader.PATHS);
            Log.d(TAG, "onStartCommand: ");
            NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int imp = NotificationManager.IMPORTANCE_HIGH;
                String name = "pdf";
                CharSequence charSequence = getString(R.string.chanel_id);
                NotificationChannel channel = new NotificationChannel(name, charSequence, imp);
                manager.createNotificationChannel(channel);
        }

            manager.notify(0, notificationSend(getApplicationContext()).build());
            downloadFromUrl(manager, args);
        }
        return START_NOT_STICKY;
    }
    /**
     * function to download a zip file from url path
     * @param arg - its a url path
     */
    @Override
    public synchronized void downloadFromUrl(final NotificationManager manager, String... arg) {
        File dir = new File(getBaseContext().getFilesDir(), "pdfs/");
        if (!dir.exists()) {
            dir.mkdir();
        }
        final File localFile = new File(dir, arg[0] + ".zip");
        StorageReference reference = FirebaseStorage.getInstance().getReference(arg[1]).child(arg[0] + ".zip");
        reference.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Log.d(TAG, "onSuccess: " + taskSnapshot.toString());
                        getFilesFromFolder(localFile);
                        manager.notify(0, notificationCloseSend(getApplicationContext()).setContentTitle("Complete").build());
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: ");
                e.printStackTrace();
                manager.notify(0, notificationCloseSend(getApplicationContext()).setContentTitle("Cannot download files").build());
            }
        });
    }

    /**
     *
     * @param path - file obj that needs to unzip
     */
    @Override
    public synchronized void getFilesFromFolder(File path) {
        ZipEntry entry;
        NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(NOTIFICATION_SERVICE);
        manager.notify(1, notificationSend(getApplicationContext()).setContentTitle("Loading").build());
        try {
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(path));
            while ((entry = zipInputStream.getNextEntry()) != null) {
                getFileFromZip(entry.getName(), path);
                zipInputStream.closeEntry();
            }
            zipInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param s - pdf name file
     * @param file - path
     * @return - full path + pdf file name
     */

    private synchronized String getFileFromZip(final String s, final File file) {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FileOutputStream stream;
                String name = s;
                File path = file;
                ZipEntry entry;
                try {
                    ZipInputStream inputStream = new ZipInputStream(new FileInputStream(path));
                    while ((entry = inputStream.getNextEntry()) != null) {
                        if (entry.getName().equals(name) && entry.getName().contains(".pdf")) {
                            String[] arr = entry.getName().split("[\\\\/]+|(.pdf)$");
                            Log.d(TAG, "run: " + path.getParent() + " name " + arr[arr.length - 1] + ".pdf");
                            stream = new FileOutputStream(path.getParent() + "/" + arr[arr.length - 1] + ".pdf");
                            byte[] buffer = new byte[2048];
                            int c = 0;
                            while ((c = inputStream.read(buffer, 0, 2048)) != -1) {
                                stream.write(buffer, 0, c);
                            }
                            inputStream.closeEntry();
                            stream.close();
                            Log.d(TAG, "run: completed");
                        }
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
        mStepExecutor.execute(runnable);
        return file.getParent() + "/" + s;
    }

    /**
     * Task executor for unziping files
     */

    private class StepExecutor implements Executor {
        private BlockingQueue<Runnable> mRunnables;
        private Runnable mTask;
        private ExecutorService mService;
        private NotificationManager mNotificationManager;

        StepExecutor(BlockingQueue<Runnable> runnables, Context context) {
            this.mRunnables = runnables;
            mService = Executors.newSingleThreadExecutor();
            mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        }
        @Override
        public synchronized void execute(@NonNull final Runnable command) {
            this.mRunnables.add(new Runnable() {
                @Override
                public void run() {
                    try {
                        command.run();
                    } finally {
                        nextTask();
                        if (mRunnables.size() == 0 && mTask == null) {
                            mNotificationManager.notify(1, notificationCloseSend(getApplicationContext()).setContentTitle("Complete").build());
                            mService.shutdown();
                            FirebaseFetchService.this.stopSelf();
                        }
                    }
                }
            });
            if (mTask == null) {
                nextTask();
            }
        }

        private synchronized void nextTask() {
            if ((mTask = this.mRunnables.poll()) != null) {
                mService.execute(mTask);
            }
        }
    }

    private NotificationCompat.Builder notificationSend(Context context) {
        NotificationCompat.Builder compat = new NotificationCompat.Builder(context, "pdf")
                .setContentText("Files Download")
                .setSmallIcon(R.mipmap.pdf_icon)
                .setContentTitle("Starts")
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setGroup("Pdfs")
                .setPriority(Notification.PRIORITY_MAX)
                .setChannelId("pdf")
                .setProgress(0,0, true);
        return compat;
    }

    private NotificationCompat.Builder notificationCloseSend(Context context) {
        NotificationCompat.Builder compat = new NotificationCompat.Builder(context, "pdf")
                .setContentText("Files Download")
                .setSmallIcon(R.mipmap.pdf_icon)
                .setAutoCancel(true)
                .setGroup("Pdfs")
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .setChannelId("pdf")
                .setProgress(0,0, false);
        return compat;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        Intent intent = new Intent(BUTTON_ENABLE);
        sendBroadcast(intent, PRIVATE);
        super.onDestroy();
    }


}
