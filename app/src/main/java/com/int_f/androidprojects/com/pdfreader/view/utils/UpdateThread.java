package com.int_f.androidprojects.com.pdfreader.view.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.CycleInterpolator;
import android.widget.TextView;

import com.int_f.androidprojects.com.pdfreader.R;

/**
 * Thread that updates a pages in toolbar
 */

public class UpdateThread extends Thread {

    private View View;
    private HandlerPager mHandlerPager;
    private static final String TAG = "UpdateThread";

    public UpdateThread(View view) {
        View = view;
    }

    @Override
    public void run() {
        Looper.prepare();
        mHandlerPager = new HandlerPager(View);
        Log.d(TAG, "run: " + Looper.myLooper().getThread().getName());
        Looper.loop();
    }

    public HandlerPager getHandlerPager() {
        return mHandlerPager;
    }

    public static class HandlerPager extends Handler {

        private View View;

        private HandlerPager(View view) {
            this.View = view;
        }

        @Override
        public void handleMessage(final Message msg) {
            if (msg.what == 3) {
                final int page = (int) msg.obj + 1;
                this.View.post(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)View).setText(R.string.pages_count);
                        ((TextView)View).append(String.valueOf(page));
                    }
                });
            }
        }
    }
}
