package com.int_f.androidprojects.com.pdfreader.view.utils;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

import com.int_f.androidprojects.com.pdfreader.R;

/**
 * class for control toolbar view state
 */

public class SimpleFling extends GestureDetector.SimpleOnGestureListener {

    private static final String TAG = "SimpleFling";

    private View mFragmentView;
    private Context mContext;
    private GestureDetector mGestureDetector;
    private WindowManager mWindowManager;
    private VelocityTracker mVelocityTracker = null;

    public SimpleFling(View fragmentView, Context context) {
        mFragmentView = fragmentView;
        mContext = context;
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        mGestureDetector = new GestureDetector(mContext, this);
        setupOnTouchFragment();
    }

    private void setupOnTouchFragment() {
        final DisplayMetrics metrics = new DisplayMetrics();
        mWindowManager.getDefaultDisplay().getMetrics(metrics);
        mFragmentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mVelocityTracker == null) {
                            mVelocityTracker = VelocityTracker.obtain();
                        } else {
                            mVelocityTracker.clear();
                        }
                        mVelocityTracker.addMovement(event);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mVelocityTracker.addMovement(event);
                        mVelocityTracker.computeCurrentVelocity(1000);
                        Log.d(TAG, "onDown: " + mVelocityTracker.getXVelocity() + " ");
                        final Button button = mFragmentView.findViewById(R.id.back_button);
                        if (mVelocityTracker.getXVelocity() > 2000) {
                            mFragmentView.animate().setInterpolator(new LinearInterpolator())
                                    .translationX(metrics.widthPixels).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    mFragmentView.setX(metrics.widthPixels - button.getBottom());
                                    button.setScaleX(0.7f); button.setScaleY(0.7f);
                                }
                            });
                            Log.d(TAG, "onTouch: " + mFragmentView.getBottom());
                        } if (mVelocityTracker.getXVelocity() < -500) {
                            mFragmentView.animate().setInterpolator(new LinearInterpolator())
                                    .translationX(0).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    mFragmentView.setX(0);
                                    button.setScaleX(1f); button.setScaleY(1f);

                                }
                            });
                    }
                        break;
                }

                return mGestureDetector.onTouchEvent(event);
            }
        });
    }


}
