package com.int_f.androidprojects.com.pdfreader.assetsProvider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * assets content provider for pdfs files to send it from intent
 */

public class PDFsAssetsProv extends ContentProvider {

    private static final String TAG = "PDFsAssetsProv";
    private String[] proj = {OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE};

    @Override
    public boolean onCreate() {
        return true;
    }
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        if (projection == null) {
            projection = proj;
        }
        final AssetManager manager = getContext().getAssets();
        final String path = getPath(uri);
        long size = 0;
        try {

            final AssetFileDescriptor afd = manager.openFd(path);
            size = afd.getLength();

        } catch (IOException e) {
            e.printStackTrace();
        }

        final String[] cols = new String[projection.length];
        final Object[] values = new Object[projection.length];
        int i = 0;
        for (String s : projection) {
            if (OpenableColumns.DISPLAY_NAME.equals(s)) {
                cols[i] = OpenableColumns.DISPLAY_NAME;
                values[i++] = uri.getLastPathSegment();
            } else if (OpenableColumns.SIZE.equals(s)) {
                cols[i] = OpenableColumns.SIZE;
                values[i++] = size;
            }
        }
        final MatrixCursor cursor = new MatrixCursor(cols, 1);
        cursor.addRow(values);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        Log.d(TAG, "getType: " + uri);
        final String file_name = uri.getLastPathSegment();
        final int lastDot = file_name.lastIndexOf('.');
        if (lastDot >= 0) {
            final String extension = file_name.substring(lastDot + 1);
            final String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            if (mime != null) {
                return mime;
            }
        }

        return "application/octet-stream";
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        throw new UnsupportedOperationException("This ContentProvider is read-only");
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        throw new UnsupportedOperationException("This ContentProvider is read-only");
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        throw new UnsupportedOperationException("This ContentProvider is read-only");
    }

    @Nullable
    @Override
    public AssetFileDescriptor openAssetFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
        AssetManager manager = getContext().getAssets();
        String path = getPath(uri);
            if (path == null) {
                throw new FileNotFoundException();
            }
        AssetFileDescriptor assetFileDescriptor = null;
        try {
            assetFileDescriptor = manager.openFd(path);
        } catch (IOException e) {
                e.printStackTrace();
        }

        return assetFileDescriptor;
    }

    private String getPath(Uri uri) {
        String[] path = uri.getPath().split("(pdfs/)");
        String file = "pdfs/" + path[path.length - 1];
        return file;
    }
}
