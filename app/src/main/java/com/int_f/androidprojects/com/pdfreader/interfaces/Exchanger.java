package com.int_f.androidprojects.com.pdfreader.interfaces;

/**
 * Interface for exchange data
 */

public interface Exchanger<T> {
    void dataLoaded(T t, T s);
}
