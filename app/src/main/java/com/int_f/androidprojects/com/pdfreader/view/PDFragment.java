package com.int_f.androidprojects.com.pdfreader.view;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.link.LinkHandler;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.model.LinkTapEvent;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import com.int_f.androidprojects.com.pdfreader.R;
import com.int_f.androidprojects.com.pdfreader.abstracts.FragmentReader;
import com.int_f.androidprojects.com.pdfreader.controllers.controllersUtils.ControllerCallback;
import com.int_f.androidprojects.com.pdfreader.model.PDFdoc;
import com.int_f.androidprojects.com.pdfreader.view.utils.SimpleFling;
import com.int_f.androidprojects.com.pdfreader.view.utils.UpdateThread;
import com.int_f.androidprojects.com.pdfreader.view.utils.VFragmensCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Single PDF view class
 */

public class PDFragment extends FragmentReader implements ControllerCallback<PDFdoc> {

    private static final String TAG = "PDFragment";
    public static final String PDFDOC_REQ = "42";
    public static final String SCROLL_REC = "3PI";
    private PDFdoc mPDFdoc;
    private PDFView mPDFView;
    private VFragmensCallback<String> mStringVFragmensCallback;
    private UpdateThread mUpdateThread;
    private Toolbar mToolbar;
    private TextView mToolBarText;
    private boolean mHorizontalMode = false;
    private static final int SEND_PAGE = 1;

    public static PDFragment getInstance(@NonNull PDFdoc s) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PDFDOC_REQ, s);
        PDFragment fragment = new PDFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PDFdoc fdoc = getArguments().getParcelable(PDFDOC_REQ);
        if (fdoc != null) {
            mPDFdoc = fdoc;
        }
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(PDFDOC_REQ)) {
                mPDFdoc = savedInstanceState.getParcelable(PDFDOC_REQ);
                mHorizontalMode = savedInstanceState.getBoolean(SCROLL_REC);
            }
        }
        mStringVFragmensCallback = (VFragmensCallback<String>) getActivity();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.pdf_fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mPDFView = v.findViewById(R.id.pdf_view);
        mToolbar = v.findViewById(R.id.tool_bar);
        SimpleFling simpleFling = new SimpleFling(mToolbar, getActivity().getBaseContext());
        TextView textView = mToolbar.findViewById(R.id.pages_count);
        mToolBarText = mToolbar.findViewById(R.id.pdf_name);
        mToolBarText.setText(mPDFdoc.getPDFname());
        final Button buttonBack = mToolbar.findViewById(R.id.back_button);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonBack.animate().scaleXBy(2f).scaleYBy(2f).setInterpolator(new AccelerateInterpolator())
                        .setDuration(300).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        buttonBack.setScaleX(1f);
                        buttonBack.setScaleY(1f);

                    }
                });
                getActivity().onBackPressed();
            }
        });
        final Button buttonShare = mToolbar.findViewById(R.id.action_share);
        buttonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonShare.setRotation(0f);
                buttonShare.animate().setDuration(500).setInterpolator(new LinearInterpolator())
                        .rotation(360f).start();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    android.support.v7.app.AlertDialog dialog = PDFShareDialog.newInstance(getActivity(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    createPage(mPDFView, mPDFdoc);
                                    break;
                                case 1:
                                    if (mPDFdoc.isFromAssets()) {
                                        sendFileFromAssets(mPDFdoc.getPDFullName());
                                    } else {
                                        sendFileFromPath(mPDFdoc.getPDFullName());
                                    }
                                    break;
                            }
                        }
                    });
                    dialog.show();

                } else {
                    if (mPDFdoc.isFromAssets()) {
                        sendFileFromAssets(mPDFdoc.getPDFullName());
                    } else {
                        sendFileFromPath(mPDFdoc.getPDFullName());
                    }
                }
            }
        });
        mUpdateThread = new UpdateThread(textView);
        mUpdateThread.setPriority(Thread.NORM_PRIORITY);
        mUpdateThread.setDaemon(true);
        mUpdateThread.start();
        loadPdfDoc(mPDFdoc);
        final RadioButton radioButton = mToolbar.findViewById(R.id.horizontal_mode);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHorizontalMode && radioButton.isChecked()) {
                    mHorizontalMode = false;
                    mPDFdoc.setCurrentPage(mPDFView.getCurrentPage());
                    loadPdfDoc(mPDFdoc);
                    radioButton.setChecked(false);
                    return;
                }
                mHorizontalMode = true;
                mPDFdoc.setCurrentPage(mPDFView.getCurrentPage());
                loadPdfDoc(mPDFdoc);
            }
        });
        Log.d(TAG, "onCreateView: ");
        return v;
    }

    @Override
    public void changeFragmentData(PDFdoc arg) {
        mPDFdoc.setCurrentPage(mPDFView.getCurrentPage());
        mStringVFragmensCallback.saveDocumentState(mPDFdoc);
        mToolBarText.setText(arg.getPDFname());
        deleteCashedFiles();
        loadPdfDoc(arg);
    }

    @Override
    public void onBackPressCallBack(PDFdoc arg) {
        mToolBarText.setText(arg.getPDFname());
        deleteCashedFiles();
        loadPdfDoc(arg);
    }

    @Override
    public void changePageInPDFdoc(Integer arg) {
        mPDFView.jumpTo(arg);
        Log.d(TAG, "changePageInPDFdoc: page back " + arg);
    }

    private void changePage(Integer page) {
        if (page != null) {
            mPDFdoc.getPagesStack().add(mPDFView.getCurrentPage());
            mPDFView.jumpTo(page);
            Log.d(TAG, "changePage: " + page);
        }
    }

    private void loadPdfDoc(PDFdoc pdFdoc) {
        if (pdFdoc != null) {
            mPDFdoc = pdFdoc;
            PDFView.Configurator configurator = null;
            if (pdFdoc.isFromAssets()) {
                configurator = mPDFView.fromAsset(mPDFdoc.getPDFullName());
            } else {
                File file = new File(pdFdoc.getPDFullName());
                if (file.exists()) {
                    configurator = mPDFView.fromFile(file);
                }
            }
            configurator
                    .enableDoubletap(true)
                    .enableAntialiasing(true)
                    .scrollHandle(new DefaultScrollHandle(getActivity()))
                    .swipeHorizontal(mHorizontalMode)
                    .defaultPage(mPDFdoc.getCurrentPage())
                    .linkHandler(new LinkHandler() {
                        @Override
                        public void handleLinkEvent(LinkTapEvent event) {
                            if (!event.getLink().getUri().contains("youtu.be/")) {
                                mStringVFragmensCallback.changeDoc(event.getLink().getUri());
                            }
                            if (event.getLink().getDestPageIdx() != null) {
                                changePage(event.getLink().getDestPageIdx());
                            }
                            if (event.getLink().getUri().contains("youtu.be/")){
                                mStringVFragmensCallback.startVideo(event.getLink().getUri());
                            }
                            Log.d(TAG, "handleLinkEvent: " + event.getLink().getDestPageIdx() + " "
                                    + event.getLink().getUri());

                        }
                    })
                    .onPageChange(new OnPageChangeListener() {
                        @Override
                        public void onPageChanged(int page, int pageCount) {
                            if (mUpdateThread != null) {
                                Message message = mUpdateThread.getHandlerPager().obtainMessage();
                                message.obj = page;
                                message.what = 3;
                                message.sendToTarget();
                            }
                        }
                    })
                    .load();
        }
    }

    private void sendFileFromPath(String s) {
        Uri uri = FileProvider.getUriForFile(getActivity(), "com.int_f.androidprojects.com.pdfreader.fileprovider",
                new File(s));
        Log.d(TAG, "sendFileFromAssets: " + uri.getLastPathSegment());
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("document/*");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivityForResult(Intent.createChooser(intent, getActivity().getResources().getString(R.string.choose_app)), SEND_PAGE);
    }

    private void sendFileFromAssets(String s) {
        Uri uri = Uri.parse("content://com.int_f.androidprojects.com.pdfreader")
                .buildUpon()
                .appendEncodedPath(s)
                .build();
        Log.d(TAG, "sendFileFromAssets: " + uri.getLastPathSegment());
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("document/*");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, getActivity().getResources().getString(R.string.choose_app)));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPDFdoc.setCurrentPage(mPDFView.getCurrentPage());
        mUpdateThread.interrupt();
        outState.putParcelable(PDFDOC_REQ, mPDFdoc);
        outState.putBoolean(SCROLL_REC, mHorizontalMode);
        Log.d(TAG, "onSaveInstanceState: ");
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        mPDFdoc.setCurrentPage(mPDFView.getCurrentPage());
        mPDFView.recycle();
        deleteCashedFiles();
        Log.d(TAG, "onDestroy: ");
    }

    private void createPage(PDFView views, PDFdoc pdFdoc) {
        try {
            File file;
            ParcelFileDescriptor descriptor;
            if (!pdFdoc.isFromAssets()) {
                file = new File(pdFdoc.getPDFullName());
            } else {
                file = copyAssetToFile(pdFdoc);
            }
            descriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
            PdfRenderer pdfRenderer = new PdfRenderer(descriptor);
            PdfRenderer.Page p = pdfRenderer.openPage(views.getCurrentPage());
            Bitmap bitmap = createBitmapForPdf(p);
            p.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            ImageView view = new ImageView(getActivity());
            int measureWidth = View.MeasureSpec.makeMeasureSpec(p.getWidth(), View.MeasureSpec.EXACTLY);
            int measuredHeight = View.MeasureSpec.makeMeasureSpec(p.getHeight(), View.MeasureSpec.EXACTLY);
            view.measure(measureWidth, measuredHeight);
            view.layout(0, 0, p.getWidth(), p.getHeight());
            view.setImageBitmap(bitmap);
            PdfDocument document = new PdfDocument();
            PdfDocument.PageInfo info = new PdfDocument.PageInfo.Builder(p.getWidth(), p.getHeight(), 0)
                    .setContentRect(new Rect(0, 0, p.getWidth(), p.getHeight())).create();
            PdfDocument.Page pe = document.startPage(info);
            view.draw(pe.getCanvas());
            document.finishPage(pe);
            p.close();
            pdfRenderer.close();
            FileOutputStream outputStream = null;
            File path = new File(getActivity().getFilesDir(), "pdfsP/");
            try {
                path.mkdir();
                Log.d(TAG, "onItemClick: " + path.getAbsolutePath());
                outputStream = new FileOutputStream(path + "/" + "page.pdf");
                document.writeTo(outputStream);
                Log.d(TAG, "onItemClick: " + document.getPages().size());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                document.close();
                try {
                    outputStream.close();
                } catch (IOException e) {
                }
            }
            sendFileFromPath(path.getAbsolutePath() + "/page.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Bitmap createBitmapForPdf(PdfRenderer.Page page) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        manager.getDefaultDisplay().getMetrics(metrics);
//        int scale = 1;
//        if (metrics.widthPixels > page.getWidth() || metrics.heightPixels > page.getHeight()) {
//            while (metrics.widthPixels > (page.getWidth() * scale) && metrics.heightPixels > (page.getHeight() * scale)) {
//                scale += 1;
//            }
//        }
        Bitmap bitmap = Bitmap.createBitmap(metrics.densityDpi * page.getWidth() / 72,
                metrics.densityDpi * page.getHeight() / 72, Bitmap.Config.ARGB_8888);
        return bitmap;
    }

    private File copyAssetToFile(PDFdoc pdFdoc) throws IOException {
        InputStream in = getActivity().getAssets().open(pdFdoc.getPDFullName());
        File file = new File(getActivity().getFilesDir(), "pdfsP/");
        if (!file.exists()) {
            file.mkdir();
        }
        File fileP = new File(file, pdFdoc.getPDFname() + ".pdf");
        if (!fileP.exists()) {
            FileOutputStream outputStream = new FileOutputStream(fileP);
            int step = 0;
            byte[] buffer = new byte[2048];
            while ((step = in.read(buffer)) != -1) {
                outputStream.write(buffer, 0, step);
            }
            in.close();
            outputStream.close();
            Log.d(TAG, "copyAssetToFile: file cashed");
        }
        return fileP;
    }

    private void deleteCashedFiles() {
        File file = new File(getActivity().getFilesDir(), "pdfsP/page.pdf");
        if (file.exists()) {
            file.delete();
            Log.d(TAG, "deleteCashedFiles: " + file.getName());
        }
        File asset = new File(getActivity().getFilesDir(), "pdfsP/" + mPDFdoc.getPDFname() + ".pdf");
        if (asset.exists()) {
            asset.delete();
            Log.d(TAG, "deleteCashedFiles: " + asset.getName());
        }
    }
}

